import argparse
from typing import TextIO


class Beacon:
    def __init__(self, location):
        x, y, z = location
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        return f'({self.x}, {self.y}, {self.z})'

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z


class Scanner:
    def __init__(self, text):
        self.beacons = []
        lines = text.split('\n')
        for line in lines[1:]:
            x, y, z = [int(t) for t in line.split(',')]
            self.beacons.append(Beacon((x, y, z)))

    def get_beacons(self):
        return self.beacons

    def add_beacon(self, beacon):
        if beacon not in self.beacons:
            self.beacons.append(beacon)


class MagnitudeMap:
    def __init__(self, scanner):
        self.mag_lists = []
        beacons = scanner.get_beacons()
        for beacon_a in beacons:
            mag_list = []
            for beacon_b in beacons:
                if beacon_a is beacon_b:
                    continue
                vector = (beacon_b[0] - beacon_a[0], beacon_b[1] - beacon_a[1], beacon_b[2] - beacon_a[2])
                mag = vector[0] ** 2 + vector[1] ** 2 + vector[2] ** 2
                mag_list.append(mag)
            self.mag_lists.append(mag_list)

    def get_map(self):
        return self.mag_lists


class RelativeInfo:
    def __init__(self, square_mag, other):
        self.square_mag = square_mag
        self.other = other

    def __repr__(self):
        return f'{self.other}: {self.square_mag}'


def make_rel_list(beacon, others):
    rel_list = []
    for other in others:
        if beacon is other:
            continue
        square_mag = (other.x - beacon.x) ** 2 + (other.y - beacon.y) ** 2 + (other.z - beacon.z) ** 2
        rel_list.append(RelativeInfo(square_mag, other))
    return rel_list


def rel_list_intersect(rel_list_1, rel_list_2):
    rel_set_1 = set([x.square_mag for x in rel_list_1])
    rel_set_2 = set([x.square_mag for x in rel_list_2])
    intersection = rel_set_1.intersection(rel_set_2)
    return len(intersection) >= 11


def get_rel_list_intersect(rel_list_a, rel_list_b):
    rel_set_a = set([x.square_mag for x in rel_list_a])
    rel_set_b = set([x.square_mag for x in rel_list_b])
    intersection = rel_set_a.intersection(rel_set_b)
    intersection_list = {x: {'a': None, 'b': None} for x in intersection}
    for rel in rel_list_a:
        if rel.square_mag in intersection:
            intersection_list[rel.square_mag]['a'] = rel
    for rel in rel_list_b:
        if rel.square_mag in intersection:
            intersection_list[rel.square_mag]['b'] = rel
    return intersection_list


def mag_list_intersect(mag_list_1, mag_list_2):
    return len(set(mag_list_1).intersection(set(mag_list_2)))


def find_common_beacon(scanner_a, scanner_b):
    for beacon_a in scanner_a.get_beacons():
        for beacon_b in scanner_b.get_beacons():
            rel_list_a = make_rel_list(beacon_a, scanner_a.get_beacons())
            rel_list_b = make_rel_list(beacon_b, scanner_b.get_beacons())
            if rel_list_intersect(rel_list_a, rel_list_b):
                temp = get_rel_list_intersect(rel_list_a, rel_list_b)
                return beacon_a, beacon_b
    return None


class Transform:
    def __init__(self, x_offset, x_flip, y_offset, y_flip, z_offset, z_flip):
        self.x_offset = x_offset
        self.x_flip = x_flip
        self.y_offset = y_offset
        self.y_flip = y_flip
        self.z_offset = z_offset
        self.z_flip = z_flip


ROTATION_LIST = [
    lambda x, y, z: (x, y, z),
    lambda x, y, z: (y, -x, z),
    lambda x, y, z: (-x, -y, z),
    lambda x, y, z: (y, x, z),
    lambda x, y, z: (-x, y, -z),
    lambda x, y, z: (y, x, -z),
    lambda x, y, z: (x, -y, -z),
    lambda x, y, z: (-y, -x, -z),
    lambda x, y, z: (-z, y, x),
    lambda x, y, z: (-y, -z, x),
    lambda x, y, z: (z, -y, x),
    lambda x, y, z: (y, z, x),
    lambda x, y, z: (z, y, -x),
    lambda x, y, z: (y, -z, -x),
    lambda x, y, z: (-z, -y, -x),
    lambda x, y, z: (-y, z, -x),
    lambda x, y, z: (x, -z, y),
    lambda x, y, z: (-z, -x, y),
    lambda x, y, z: (-x, z, y),
    lambda x, y, z: (z, x, y),
    lambda x, y, z: (-x, -z, -y),
    lambda x, y, z: (-z, x, -y),
    lambda x, y, z: (x, z, -y),
    lambda x, y, z: (z, -x, -y),
]


class Transform2:
    def __init__(self, rotation, offset):
        self.rotation = rotation
        self.offset = offset


def apply_transform2(tf, beacon):
    x = beacon.x
    y = beacon.y
    z = beacon.z
    rotated = ROTATION_LIST[tf.rotation](x, y, z)
    return Beacon((rotated[0] + tf.offset[0], rotated[1] + tf.offset[1], rotated[2] + tf.offset[2]))


def apply_rotate(rotation, beacon):
    x = beacon.x
    y = beacon.y
    z = beacon.z
    rotated = ROTATION_LIST[rotation](x, y, z)
    return Beacon(rotated)


def apply_transform(tf, beacon):
    x_scale = -1 if tf.x_flip else 1
    y_scale = -1 if tf.y_flip else 1
    z_scale = -1 if tf.z_flip else 1
    x = beacon.x
    y = beacon.y
    z = beacon.z
    return Beacon((x * x_scale + tf.x_offset, y * y_scale + tf.y_offset, z * z_scale + tf.z_offset))


def get_transform(scanner_a, scanner_b):
    for beacon_a in scanner_a.get_beacons():
        for beacon_b in scanner_b.get_beacons():
            rel_list_a = make_rel_list(beacon_a, scanner_a.get_beacons())
            rel_list_b = make_rel_list(beacon_b, scanner_b.get_beacons())
            if rel_list_intersect(rel_list_a, rel_list_b):
                intersection = get_rel_list_intersect(rel_list_a, rel_list_b)
                # x_offsets = get_offset_possibilities(beacon_a.x, beacon_b.x)
                # y_offsets = get_offset_possibilities(beacon_a.y, beacon_b.y)
                # z_offsets = get_offset_possibilities(beacon_a.z, beacon_b.z)
                #
                # x_offsets = [x for x in x_offsets if get_flip(x, beacon_b.x, beacon_a.x) is not None]
                # y_offsets = [y for y in y_offsets if get_flip(y, beacon_b.y, beacon_a.y) is not None]
                # z_offsets = [z for z in z_offsets if get_flip(z, beacon_b.z, beacon_a.z) is not None]
                #
                # transforms = []
                # for x_offset in x_offsets:
                #     for y_offset in y_offsets:
                #         for z_offset in z_offsets:
                #             transforms.append(Transform(x_offset, get_flip(x_offset, beacon_b.x, beacon_a.x), y_offset, get_flip(y_offset, beacon_b.y, beacon_a.y), z_offset, get_flip(z_offset, beacon_b.z, beacon_a.z)))
                # check = list(intersection.values())[0]
                # for transform in transforms:
                #     if apply_transform(transform, beacon_b) != beacon_a:
                #         continue
                #     if apply_transform(transform, check['b'].other) == check['a'].other:
                #         return transform
                # raise RuntimeError('No transform found')
                check = list(intersection.values())[0]
                for rotation in range(len(ROTATION_LIST)):
                    rotated = apply_rotate(rotation, beacon_b)
                    offset_x = beacon_a.x - rotated.x
                    offset_y = beacon_a.y - rotated.y
                    offset_z = beacon_a.z - rotated.z
                    transform = Transform2(rotation, (offset_x, offset_y, offset_z))
                    if apply_transform2(transform, check['b'].other) == check['a'].other:
                        return transform


    return None


def get_offset_possibilities(a, b):
    return [b - a, b + a, -b - a, -b + a]


def get_flip(offset, source, destination):
    if source + offset == destination:
        return False  # No flip
    if -source + offset == destination:
        return True  # Flip
    return None  # No possibility


def part_1(input_file: TextIO):
    scanner_texts = input_file.read().split('\n\n')
    scanners = []
    for scanner_text in scanner_texts:
        scanners.append(Scanner(scanner_text))

    main_scanner = scanners[0]
    scanners = scanners[1:]
    while len(scanners) > 0:
        merged = False
        for scanner in scanners:
            transform = get_transform(main_scanner, scanner)
            if transform is None:
                continue
            for beacon in scanner.get_beacons():
                main_scanner.add_beacon(apply_transform2(transform, beacon))
            scanners.remove(scanner)
            print('Merged')
            merged = True
            break
        if not merged:
            print('No good!')
    return len(main_scanner.get_beacons())
    # scanner_a = scanners[0]
    # scanner_b = scanners[1]
    # return get_transform(scanner_a, scanner_b)

    # mag_list_a = MagnitudeMap(scanner_a)
    # mag_list_b = MagnitudeMap(scanner_b)
    # return mag_list_intersect(mag_list_a.get_map()[9], mag_list_b.get_map()[0])


def part_2(input_file: TextIO):
    return 0


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 19')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
